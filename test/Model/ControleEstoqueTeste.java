/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author arthur
 */
public class ControleEstoqueTeste {
    
    public ControleEstoqueTeste() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testGetQuantidadeMinima() {         
        
        System.out.println("Teste getQuantidadeMinima");         
        ControleEstoque instancia = new ControleEstoque();         
        
        int expResult = 10;         
        instancia .setQuantidadeMinima(10);         
        int result = instancia .getQuantidadeMinima();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetQuantidadeMinima() {         
        
        System.out.println("Teste setQuantidadeMinima");         
        int quantidade = 10;         
        ControleEstoque instancia = new ControleEstoque();         
        instancia.setQuantidadeMinima(quantidade);     
    }
    
    
    
    @Test
    public void testGetQuantidadeComida() {         
        
        System.out.println("Teste getQuantidadeComida");         
        ControleEstoque instancia = new ControleEstoque();         
        
        int expResult = 10;         
        instancia .setQuantidadeComida(10);         
        int result = instancia .getQuantidadeMinima();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetQuantidadeComida() {         
        
        System.out.println("Teste setQuantidadeComida");         
        int quantidadeComida = 10;         
        ControleEstoque instancia = new ControleEstoque();         
        instancia.setQuantidadeBebida(quantidadeComida);     
    }
    
    
    
    
    @Test
    public void testGetQuantidadeBebida() {         
        
        System.out.println("Teste getQuantidadeBebida");         
        ControleEstoque instancia = new ControleEstoque();         
        
        int expResult = 10;         
        instancia .setQuantidadeBebida(10);         
        int result = instancia .getQuantidadeBebida();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetQuantidadeBebida() {         
        
        System.out.println("Teste setQuantidadeBebida");         
        int quantidadeBebida = 10;         
        ControleEstoque instancia = new ControleEstoque();         
        instancia.setQuantidadeBebida(quantidadeBebida);     
    }
    
    @Test
    public void testGetQuantidadeSobremesa() {         
        
        System.out.println("Teste getQuantidadeSobremesa");         
        ControleEstoque instancia = new ControleEstoque();         
        
        int expResult = 10;         
        instancia .setQuantidadeSobremesa(10);         
        int result = instancia.getQuantidadeSobremesa();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetQuantidadeSobremesa() {         
        
        System.out.println("Teste setQuantidadeSobremesa");         
        int quantidadeSobremesa = 10;         
        ControleEstoque instancia = new ControleEstoque();         
        instancia.setQuantidadeSobremesa(quantidadeSobremesa);     
    }
    
    @Test 
    public void testAumentaQuantidadeComida(){
        
        System.out.println("Teste aumentaQuantidadeComida");         
        ControleEstoque instancia = new ControleEstoque();      
        
        int expResult = instancia.getQuantidadeComida()+5;
        instancia.aumentaQuantidadeComida(5); 
        int result = instancia.getQuantidadeComida();
        assertEquals(expResult, result);  
    }
    
    @Test 
    public void testAumentaQuantidadeBebida(){
        
        System.out.println("Teste aumentaQuantidadeBebida");         
        ControleEstoque instancia = new ControleEstoque();      
        
        int expResult = instancia.getQuantidadeBebida()+5;
        instancia.aumentaQuantidadeBebida(5); 
        int result = instancia.getQuantidadeBebida();
        assertEquals(expResult, result);  
    }
    
    @Test 
    public void testAumentaQuantidadeSobremesa(){
        
        System.out.println("Teste aumentaQuantidadeSobremesa");         
        ControleEstoque instancia = new ControleEstoque();      
        
        int expResult = instancia.getQuantidadeSobremesa()+5;
        instancia.aumentaQuantidadeSobremesa(5); 
        int result = instancia.getQuantidadeSobremesa();
        assertEquals(expResult, result);  
    }
    
     @Test 
    public void testDiminuiQuantidadeComida(){
        
        System.out.println("Teste diminuiQuantidadeComida");         
        ControleEstoque instancia = new ControleEstoque();      
        
        int expResult = instancia.getQuantidadeComida()-5;
        instancia.diminuiQuantidadeComida(5); 
        int result = instancia.getQuantidadeComida();
        assertEquals(expResult, result);  
    }
    
    @Test 
    public void testDiminuiQuantidadeBebida(){
        
        System.out.println("Teste diminuiQuantidadeBebida");         
        ControleEstoque instancia = new ControleEstoque();      
        
        int expResult = instancia.getQuantidadeBebida()-5;
        instancia.diminuiQuantidadeBebida(5); 
        int result = instancia.getQuantidadeBebida();
        assertEquals(expResult, result);  
    }
    
    @Test 
    public void testDiminuiQuantidadeSobremesa(){
        
        System.out.println("Teste diminuiQuantidadeSobremesa");         
        ControleEstoque instancia = new ControleEstoque();      
        
        int expResult = instancia.getQuantidadeSobremesa()-5;
        instancia.diminuiQuantidadeSobremesa(5); 
        int result = instancia.getQuantidadeSobremesa();
        assertEquals(expResult, result);  
    }
    
    
}
