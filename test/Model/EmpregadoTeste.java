/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author arthur
 */
public class EmpregadoTeste {
    
    public EmpregadoTeste() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testGetLogin() {         
        
        System.out.println("Teste getLogin");         
        Empregado instancia = new Empregado();         
        
        String expResult = "login";         
        instancia .setLogin("login");         
        String result = instancia .getLogin();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetLogin() {         
        
        System.out.println("Teste setLogin");         
        String login = "login";         
        Empregado instancia = new Empregado();         
        instancia.setLogin(login);     
    }
    
    @Test
    public void testGetSenha() {         
        
        System.out.println("Teste getSenha");         
        Empregado instancia = new Empregado();         
        
        String expResult = "senha";         
        instancia .setSenha("senha");         
        String result = instancia .getSenha();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetSenha() {         
        
        System.out.println("Teste setSenha");         
        String senha = "senha";         
        Empregado instancia = new Empregado();         
        instancia.setSenha(senha);     
    }
}
