/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author arthur
 */
public class ProdutoTeste {
    
    public ProdutoTeste() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testGetIndex() {         
        
        System.out.println("Teste getIndex");         
        Produto instancia = new Produto();         
        
        int expResult = 1;         
        instancia.setIndex(1);         
        int result = instancia.getIndex();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetIndex() {         
        
        System.out.println("Teste setIndex");         
        int index = 1;         
        Produto instancia = new Produto();         
        instancia.setIndex(index);     
    }
    
    
    @Test
    public void testGetPreco() {         
        
        System.out.println("Teste getPreco");         
        Produto instancia = new Produto();         
        
        float expResult = 10;         
        instancia .setPreco(10);         
        float result = instancia.getPreco();         
        assertEquals((int)expResult, (int)result);     
    }         
    
    @Test     
    public void testSetPreco() {         
        
        System.out.println("Teste setPreco");         
        float preco = 10;         
        Produto instancia = new Produto();         
        instancia.setPreco(preco);     
    }
    
    @Test
    public void testGetNome() {         
        
        System.out.println("Teste getNome");         
        Produto instancia = new Produto();         
        
        String expResult = "nome";         
        instancia .setNome("nome");         
        String result = instancia.getNomeProduto();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetNome() {         
        
        System.out.println("Teste setSenha");         
        String nome = "nome";         
        Produto instancia = new Produto();         
        instancia.setNome(nome);     
    }
}
