/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author arthur
 */
public class ClienteTeste {
    
    public ClienteTeste() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    
    @Test
    public void testGetNome() {         
        
        System.out.println("Teste getNome");         
        Cliente instancia = new Cliente();         
        
        String expResult = "nome";         
        instancia .setNome("nome");         
        String result = instancia .getNome();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetNome() {         
        
        System.out.println("Teste setSenha");         
        String nome = "nome";         
        Cliente instancia = new Cliente();         
        instancia.setNome(nome);     
    }
}
