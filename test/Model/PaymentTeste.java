/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author arthur
 */
public class PaymentTeste {
    
    public PaymentTeste() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testGetPagamento() {         
        
        System.out.println("Teste getPagamento");         
        Payment instancia = new Payment();         
        
        boolean expResult = true;         
        instancia .setPagamento(true);         
        boolean result = instancia .getPagamento();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetPagamento() {         
        
        System.out.println("Teste setPagamento");         
        boolean pago = true;         
        Payment instancia = new Payment();          
        instancia.setPagamento(pago);     
    }
    
    @Test
    public void testGetTipoPagamento() {         
        
        System.out.println("Teste getTipoPagamento");         
        Payment instancia = new Payment();         
        
        String expResult = "tipo";         
        instancia .setTipoPagamento("tipo");         
        String result = instancia .getTipoPagamento();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetTipoPagamento() {         
        
        System.out.println("Teste setTipoPagamento");         
        String tipo = "tipo";         
        Payment instancia = new Payment();         
        instancia.setTipoPagamento(tipo);     
    }
}
