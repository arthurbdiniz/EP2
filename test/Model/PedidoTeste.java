/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author arthur
 */
public class PedidoTeste {
    
    public PedidoTeste() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testGetQuantidadeComida() {         
        
        System.out.println("Teste getQuantidadeComida");         
        Pedido instancia = new Pedido();         
        
        int expResult = 10;         
        instancia.setQuantidadeComida(10);         
        int result = instancia.getQuantidadeComida();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetQuantidadeComida() {         
        
        System.out.println("Teste setQuantidadeComida");         
        int quantidade = 10;         
        Pedido instancia = new Pedido();         
        instancia.setQuantidadeComida(quantidade);     
    }
    
    @Test
    public void testGetQuantidadeBebida() {         
        
        System.out.println("Teste getQuantidadeBebida");         
        Pedido instancia = new Pedido();         
        
        int expResult = 10;         
        instancia.setQuantidadeBebida(10);         
        int result = instancia.getQuantidadeBebida();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetQuantidadeBebida() {         
        
        System.out.println("Teste setQuantidadeBebida");         
        int quantidade = 10;         
        Pedido instancia = new Pedido();         
        instancia.setQuantidadeBebida(quantidade);     
    }
    @Test
    public void testGetQuantidadeSobremesa() {         
        
        System.out.println("Teste getQuantidadeSobremesa");         
        Pedido instancia = new Pedido();         
        
        int expResult = 10;         
        instancia.setQuantidadeSobremesa(10);         
        int result = instancia.getQuantidadeSobremesa();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetQuantidadeSobremesa() {         
        
        System.out.println("Teste setQuantidadeSobremesa");         
        int quantidade = 1;         
        Pedido instancia = new Pedido();         
        instancia.setQuantidadeSobremesa(quantidade);     
    }
    
    
    
    
    
    
    @Test
    public void testGetTotal() {         
        
        System.out.println("Teste getTotal");         
        Pedido instancia = new Pedido();         
        
        float expResult = 30;         
        instancia.setTotal(10,1,10,1,10,1);         
        float result = instancia.getTotal();         
        assertEquals((int)expResult, (int)result);     
    }         
    
    @Test     
    public void testSetTotal() {         
        
        System.out.println("Teste setTotal");         
                
        Pedido instancia = new Pedido();         
        instancia.setTotal(10,1,10,1,10,1);     
    }
    
    
    @Test
    public void testGetObsercacao() {         
        
        System.out.println("Teste getObsercacao");         
        Pedido instancia = new Pedido();        
        
        String expResult = "observacao";         
        instancia.setObservacao("observacao");         
        String result = instancia.getObservacao();         
        assertEquals(expResult, result);     
    }         
    
    @Test     
    public void testSetObsercacao() {         
        
        System.out.println("Teste setObsercacao");         
        String obs = "observacao";         
        Pedido instancia = new Pedido();         
        instancia.setObservacao(obs);     
    }
}
