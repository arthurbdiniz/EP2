# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.


## Profº Renato

## Aluno: Arthur Barbosa Diniz - 15/0118457

## Neste arquivo deve conter as instruções de execução e descrição do projeto.

Para compilar e executar, siga as seguintes instruções:

    Abra o terminal
    Entre em um diretório onde ira salvar o projeto: $ cd EP2/
    De um clone no repositorio: *$ git clone https://gitlab.com/arthurbdiniz/EP2.git
    Abra o NetBeans 
    Vá em Arquivo, Abrir Projeto
    Apos abrir o projeto abra a pasta raiz dos pacotes de Codigos-fonte
    Para executar aperte f6 ou na seta verde no canto superior da janela do NetBeans
    
Execução/Tutorial

    Ao abrir o programa pela primeira vez a senha e o usuario vao estar gravados como "admin" e "admin"
    Apos digitar o usuario e a senha aperte Login, caso esteja cadastrado no sistema será levado para o menu
    No menu irao ter 4 opcoes Cadastrar Pedido, Estoque, Trocar Senha, Cadastrar Funcionario
    Ao clicar em Cadastrar Pedido o funcionario podera escolher entre 3 pratos diferentes colocando a quantidade de cada um 
    Alem disso é necessario colocar o nome do cliente, a forma de pagamento e marcar o campo pago para efetuar a venda
    Caso escolha a opcao de Trocar senha basta colocar sua senha atual e a nova senha nos campos marcado e sera trocada na automaticamente
    Caso escolha a opcao de Cadastrar um novo funcionario basta colocar seu login e senha que sera cadastrado no "Banco de Dados" do sistema
    E por ultimo na opcao de Estoque é possivel verificar a quantidade dos produtos no estoque e pedir caso seja menor do que o recomendado pelo sistema

    
Arquivos, pastas e imagens

    Os arquivos das imagens usadas no projeto estao salvas na pasta View
    Na pasta raiz do projeto foi criada um arquivo login.txt onde estao armazenados o usuario e a senha dos funcionarios
    Caso na hora da execucao do login pela primeira vez verifique se no arquivo login.txt nao possui uma linha apos o primeiro funcionario
    Ex: admin;admin(\n)<-
    
    
