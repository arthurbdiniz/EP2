/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Cliente;
import Model.ControleEstoque;


public class ControllerCadastroPedido {
    
    
    
    private Cliente cliente;
    private ControleEstoque controleEstoque;
    
    public ControllerCadastroPedido(Cliente cliente, ControleEstoque controleEstoque){
        this.cliente = cliente; 
        this.controleEstoque = controleEstoque;
    }
    
    
    
    public boolean verificaEstoqueComida(){
        
        if(controleEstoque.getQuantidadeComida() < controleEstoque.getQuantidadeMinima()){
            return true;
            
        }else{
            return false;
        }
       
    }
    
    public boolean verificaEstoqueBebida(){
       
        if(controleEstoque.getQuantidadeBebida() < controleEstoque.getQuantidadeMinima()){
            return true;
            
        }else{
            return false;
        }
    }
    
    public boolean verificaEstoqueSobremesa(){
        
        if(controleEstoque.getQuantidadeSobremesa() < controleEstoque.getQuantidadeMinima()){
            return true;
            
        }else{
            return false;
        }
    }
    
    
    
}
