/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Empregado;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Arquivo {
    
    String usuario;
    String senha;
    private Empregado funcionario;
    
    public Arquivo(Empregado funcionario){
		this.funcionario = funcionario;
    }
    
    public Arquivo(){
        
        
    }
    
    
    public String getUsuario(){
		return usuario;
    }
    public String getSenha(){
		return senha;
    }
    
       
    
    /**
    Valida se o usuario e a senhadigitados pelo usuario estao corretos
     * @return 
     */
    public boolean validaLogin(){
         
        
        try (FileReader arq = new FileReader("login.txt")) {
            
            BufferedReader lerArq = new BufferedReader(arq);
 
            String linha = lerArq.readLine(); // lê a primeira linha
           
            
            while (linha != null) {
                
                usuario = linha.substring(0, linha.indexOf(';'));
                senha = linha.substring(linha.lastIndexOf(';') + 1, linha.length());
                
                if(usuario.equals(funcionario.getLogin())){
                    
                    
                    System.out.println("Usuario: " + usuario);                  
                    System.out.println("Senha: " +senha);
                    
                                 
                    break;
                
                    //linha = lerArq.readLine(); 
                }
                linha = lerArq.readLine(); 
            }
        }catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
            e.getMessage());
        }
 
        
        
        if(funcionario.getLogin().equals(usuario) && funcionario.getSenha().equals(senha)){
        
            return true;
        }else{
            return false;
        }
    
  
    }
    
    public void cadastraFuncionario(String usuario, String senha){
        
        File arquivo = new File("login.txt");
 
        try {
 
        if (!arquivo.exists()) {
            //cria um arquivo (vazio) caso nao ache..
            arquivo.createNewFile();
        }
 
        //Caso seja um diretório, é possível listar seus arquivos e diretórios
        File[] arquivos = arquivo.listFiles();
 
        //Escreve no arquivo
        FileWriter fw = new FileWriter(arquivo, true);
 
        BufferedWriter bw = new BufferedWriter(fw);
 
      
        bw.write(usuario);
        bw.write(";");
        bw.write(senha);
        bw.newLine();
        
       
        bw.close();
        fw.close(); 
        }
        catch(Exception e){
            
            
        }
        
        
    }
    
    public void trocarSenha(String novaSenha){
        
        
        try {
            
            
            FileReader arq = new FileReader("login.txt");
            File arq_aux = new File("aux.txt");
            
            //Caso seja um diretório, é possível listar seus arquivos e diretórios
            File[] arquivos = arq_aux.listFiles();
 
            //Escreve no arquivo
            FileWriter fw = new FileWriter(arq_aux, true);
 
            BufferedWriter bw = new BufferedWriter(fw);
            
            if (!arq_aux.exists()){
                //cria um arquivo (vazio) caso nao ache..
                arq_aux.createNewFile();
            }
 
            
            BufferedReader lerArq = new BufferedReader(arq);
            
            String linha = lerArq.readLine(); // lê a primeira linha
            
            
            
            while (linha != null) {
                
                usuario = linha.substring(0, linha.indexOf(';'));
                senha = linha.substring(linha.lastIndexOf(';') + 1, linha.length());
                
                System.out.println("Usuario: " + usuario);                  
                System.out.println("Senha: " + senha);
                
                if(usuario.equals(funcionario.getLogin())){
                      

                    bw.write(usuario);
                    bw.write(";");
                    bw.write(novaSenha);
                    bw.newLine();
        

                    System.out.println("Usuario: " + usuario);                  
                    System.out.println("Nova senha: " +novaSenha);
                    

                }else{
                    
                       
                    
                    bw.write(usuario);
                    bw.write(";");
                    bw.write(senha);
                    bw.newLine();
                    
                            
                    
                    
                    
                    
                }
                
                linha = lerArq.readLine(); 
                
            }
            
            bw.close();
            fw.close();
             
            
            File arq_sub = new File("login.txt");
            
            
            
            
            arq_aux.renameTo(arq_sub);	
            //arq_sub.delete();
             
             
        }catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",
            e.getMessage());
        }
 
        
    }   
 
        
    
    
    
    public boolean confereTrocaSenha(String senhaCliente, String senhaAntiga){
        
        if(senhaCliente.equals(senhaAntiga)){
            return true;
        }else{
            return false;
        }
        
        
        
    }
    

}