/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author arthur
 */
public class ControleEstoque {
    
    
    
    int quantidade_minima;
    int quantidade_comida;
    int quantidade_bebida;
    int quantidade_sobremesa;
    
    
    public ControleEstoque(){//Contrutor
        this.quantidade_minima = 10;
        this.quantidade_bebida = 15;
        this.quantidade_comida = 15;
        this.quantidade_sobremesa = 15;
        
    }
    
    public int getQuantidadeMinima(){
		return quantidade_minima;
    }
    public void setQuantidadeMinima(int quantidade_minima){
        this.quantidade_minima = quantidade_minima;
        
    }
    
    public int getQuantidadeComida(){
		return quantidade_comida;
    }
    public void setQuantidadeComida(int quantidade_comida){
        this.quantidade_comida = quantidade_comida;
        
    }
    
    public int getQuantidadeBebida(){
		return quantidade_bebida;
    }
    public void setQuantidadeBebida(int quantidade_bebida){
        this.quantidade_bebida = quantidade_bebida;
        
    }
    
    public int getQuantidadeSobremesa(){
		return quantidade_sobremesa;
    }
    public void setQuantidadeSobremesa(int quantidade_sobremesa){
        this.quantidade_sobremesa = quantidade_sobremesa;
        
    }
       
   
    public void aumentaQuantidadeComida(int quantidade){
        quantidade_comida += quantidade;
    }
    public void aumentaQuantidadeBebida(int quantidade){
        quantidade_bebida += quantidade;
    }
    public void aumentaQuantidadeSobremesa(int quantidade){
        quantidade_sobremesa += quantidade; 
    }
    
    public void diminuiQuantidadeComida(int quantidade){
        quantidade_comida -= quantidade;
    }
    public void diminuiQuantidadeBebida(int quantidade){
        quantidade_bebida -= quantidade;
    }
    public void diminuiQuantidadeSobremesa(int quantidade){
        quantidade_sobremesa -= quantidade; 
    }
    
    
     public boolean verificaEstoqueComida(){
        
        if(quantidade_comida < quantidade_minima){
            return true;
            
        }else{
            return false;
        }
       
    }
    
    public boolean verificaEstoqueBebida(){
       
        if(quantidade_bebida < quantidade_minima){
            return true;
            
        }else{
            return false;
        }
    }
    
    public boolean verificaEstoqueSobremesa(){
        
        if(quantidade_sobremesa < quantidade_minima){
            return true;
            
        }else{
            return false;
        }
    }
    
    
      
}
