/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;


public class Produto {
    
    private String nome;
    private int index;
    private float preco;
    
    private Bebida objBebida;
    private Comida objComida;
    private Sobremesa objSobremesa;
    
    public Produto(){
        
            
    
        
    }
    
    public Produto(Bebida objBebida, Comida objComida, Sobremesa objSobremesa){
        
        this.objBebida = objBebida;
        this.objComida = objComida;
        this.objSobremesa = objSobremesa;
        
    }
    
    
    
    public int getIndex(){
		return index;
    }
    public void setIndex(int index){
        this.index =index;
        
    }
    
    
    
    public String getNomeProduto(){
		return nome;
    }
    public void setNome(String nome){
        this.nome = nome;
        
    }
    
     public float getPreco(){
		return preco;
    }
    public void setPreco(float preco){
        this.preco = preco;
        
    }
    
}
