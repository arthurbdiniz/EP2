/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Controller.Arquivo;
import View.Login;


public class Main {
    
    
    public static void main(String[] args) {
        Empregado funcionario = new Empregado();
        Arquivo verificacao = new Arquivo(funcionario);
        
        java.awt.EventQueue.invokeLater(() -> {
            new Login(funcionario, verificacao).setVisible(true);
        });
        
        

    }
    
    
}
