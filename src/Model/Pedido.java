/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author arthur
 */
public class Pedido {
    
    //Atributos
    
    
    private Bebida objBebida;
    private Comida objComida;
    private Sobremesa objSobremesa ;
    private Payment pagamento;
    
    private ControleEstoque estoque;
    
    float total;
    int quantidade_pedida_comida;
    int quantidade_pedida_bebida;
    int quantidade_pedida_sobremesa;
    String observacao;
   
    
    //Sobrecarga do Construtor
    
    public Pedido(Bebida objBebida, Comida objComida,Sobremesa objSobremesa, Payment pagamento, ControleEstoque estoque){
        this.objBebida = objBebida;
        this.objComida = objComida;
        this.objSobremesa = objSobremesa;
        this.pagamento = pagamento;
        this.estoque = estoque;
        
    }
    
    public Pedido(){
        
    }
    
    
    //Setando as Classes Produto, Pagamento e Estoque
    
    public void setBebida(Bebida objBebida){
        this.objBebida = objBebida;
    }
    
    public void setComida(Comida objComida){
        this.objComida = objComida;
    }
    
    public void setSobremesa(Sobremesa objSobremesa){
        this.objSobremesa = objSobremesa;
    }
    
    
    public void setPagamento(Payment pagamento){
        this.pagamento = pagamento;
    }
    
    public void setEstoque(ControleEstoque estoque){
        this.estoque = estoque;
    }
    
    
    
    //Set do resto dos atributos da classe;
    

    
    public int getQuantidadeComida(){
	return quantidade_pedida_comida;
    }

    public void setQuantidadeComida(int quantidade_pedida_comida){
        this.quantidade_pedida_comida = quantidade_pedida_comida;
        
    }
    
    public int getQuantidadeBebida(){
	return quantidade_pedida_bebida;
    }

    public void setQuantidadeBebida(int quantidade_pedida_bebida){
        this.quantidade_pedida_bebida = quantidade_pedida_bebida;
        
    }
    
    public int getQuantidadeSobremesa(){
	return quantidade_pedida_sobremesa;
    }

    public void setQuantidadeSobremesa(int quantidade_pedida_sobremesa){
        this.quantidade_pedida_sobremesa = quantidade_pedida_sobremesa;
        
    }
    
  
    public float getTotal(){
	return total;
    }

    public void setTotal(float valor_bebida, int quantidade_bebida, 
            float valor_comida, int quantidade_comida, 
            float valor_sobremesa, int quantidade_sobremesa){
        
        this.total = (valor_bebida*quantidade_bebida) + 
                (valor_comida*quantidade_comida) + 
                (valor_sobremesa*quantidade_sobremesa);
        
    }
   
    
    public String getObservacao(){
	return observacao;
    }

    public void setObservacao(String observacao){
        this.observacao = observacao;
        
    }
    
    
    
    
    
}
